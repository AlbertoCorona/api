var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser=require('body-parser')
app.use(bodyParser.json())

app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

var requestjson =require('request-json');
var path = require('path');
//var urlmovimientosMlab ="https://api.mlab.com/api/1/databases/dbbanca2dacs/collections/movimientos?apiKey=I4UgqGzgO7-nI24BtSOHBFLHj9UpgM-5";
var urlmovimientosMlab ="https://api.mlab.com/api/1/databases/dbbanca2dacs/collections/customer?apiKey=I4UgqGzgO7-nI24BtSOHBFLHj9UpgM-5";
var urlMlabBase="https://api.mlab.com/api/1/databases/dbbanca2dacs/collections/customer/"
var MlabKey="?apiKey=I4UgqGzgO7-nI24BtSOHBFLHj9UpgM-5"

var clienteMlab= requestjson.createClient(urlmovimientosMlab)

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get ("/movimientos", function(req,res) {
  clienteMlab.get ('',function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.get ("/clientes/:idcliente",function(req,res) {
res.send("Aqui tiene al Cliente numero: "+ req.params.idcliente)
})


app.get("/busca/:id", function(req,res){
  var idcliente = req.params.id
  //var query = '&q={"idCustomer":'+idcliente+'}&f={"_id":1}'
  var query = '&q={"idCustomer":'+idcliente+'}'
  var bucaCliente=urlmovimientosMlab+query
  var clienteMlabFind=requestjson.createClient(bucaCliente);
  clienteMlabFind.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.get("/buscaId/:id", function(req,res){
  var idcliente = req.params.id
  var query = '&q={"idCustomer":'+idcliente+'}'
  var bucaCliente=urlmovimientosMlab+query
  var clienteMlabFindID=requestjson.createClient(bucaCliente);
  clienteMlabFindID.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

//ID maximo
app.get("/maxid", function(req,res){
  var query ='&s={"idCustomer":-1}&f={"idCustomer":1}&l=1'
  var clienteMlabMax=requestjson.createClient(urlmovimientosMlab+query);
  clienteMlabMax.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})


//Alta Cliente
app.post("/altacliente",function(req, res){
  var clienteMlabLoad= requestjson.createClient(urlmovimientosMlab)
  clienteMlabLoad.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})

app.post("/", function(req, res){
  res.send("Hemos recibido su peticiòn actualizada");
})

//Actualiza
app.put("/actualiza/:id", function(req,res){
  var idcliente = req.params.id
  var query = '&q={"idCustomer":'+idcliente+'}'
  var clienteMlabAct=requestjson.createClient(urlmovimientosMlab+query);
  var body = JSON.parse('{"$set":'+JSON.stringify(req.body)+'}')
  clienteMlabAct.put('', body, function(err, resM, body){
    res.send(body);
  })
})

app.delete("/delete/:id", function(req, res){
   var id = req.params.id
   var urltotal= urlMlabBase+id+MlabKey
   var clienteMlabDel = requestjson.createClient(urltotal)
   clienteMlabDel.delete('', function(err, resM, body) {
     res.send();
   })
   })
